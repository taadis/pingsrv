package main

import (
	"context"

	"github.com/micro/go-micro/v2"
)

type Ping struct{}

func (p *Ping) Pong(ctx context.Context, request *string, response *string) error {
	*response = "pong"
	return nil
}

func main() {
	// create new service
	service := micro.NewService(
		micro.Name("com.taadis.srv"),
	)

	// initialise command line
	service.Init()

	// set the handler
	micro.RegisterHandler(service.Server(), new(Ping))

	// run service
	service.Run()
}
