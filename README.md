# Ping Service

## 运行服务

```
go run main.go
```

```
2020-04-15 15:50:54  level=info Starting [service] com.taadis.srv
2020-04-15 15:50:54  level=info Server [grpc] Listening on [::]:36251
2020-04-15 15:50:54  level=info Registry [mdns] Registering node: com.taadis.srv-fff96af0-3633-42d3-881a-1a94f5b36a5d

```

## 调用服务


```
micro call com.taadis.srv Ping.Pong null
```

```
"pong"
```
